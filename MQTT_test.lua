led = 4
gpio.mode(led,gpio.OUTPUT,gpio.PULLUP)
wifi.setmode(wifi.STATION)
wifi.sta.config({ssid="@PHICOMM_DC",pwd="123321000"})
wifi.sta.connect()
--for i = 0,20 do
--    gpio.write(led,gpio.LOW)
--    tmr.delay(1000*1000)
--    gpio.write(led,gpio.HIGH)
--    tmr.delay(1000*1000)
--end
m = mqtt.Client("light", 120, "demo", "edu")
m:on("connect", function(client) print ("connected") end)
m:on("offline", function(client) print ("offline") end)
m:on("message", function(client, topic, data)
  
     if data ~= nil then
        print(topic .. ":" .. data )
        if data == "open" then
          gpio.write(led,gpio.LOW)
        elseif data == "close" then 
         gpio.write(led,gpio.HIGH)
        end
     end
    end)
m:on("overflow", function(client, topic, data)
  print(topic .. " partial overflowed message: " .. data )
end)

t = tmr.create()
t:register(1000,tmr.ALARM_AUTO,function()
    if wifi.sta.getip() == nil then
        print("connecting...")
    else 
        print("connected,Ip is "..wifi.sta.getip()) 
        t:stop()
		t:register(2000,tmr.ALARM_AUTO,main)
		t:start()
        m:connect("192.168.2.136", 1883, 0, function(client)
            print("connected")
            a = client

  
            client:subscribe("test", 0, function(client) print("subscribe success") end)
 
            client:publish("test", "hello", 0, 0, function(client) print("sent") end)
end,
function(client, reason)
  print("failed reason: " .. reason)
end)   
    end
end)
t:start()
function main()
	status, temp, humi, temp_dec, humi_dec = dht.read11(6)
	if status == dht.OK then
    -- Integer firmware using this example
		print(string.format("DHT Temperature:%d.%03d;Humidity:%d.%03d\r\n",
			math.floor(temp),
			temp_dec,
			math.floor(humi),
			humi_dec
		))

    -- Float firmware using this example
--		print("DHT Temperature:"..temp..";".."Humidity:"..humi)
        
        ok, json = pcall(sjson.encode, {temp=temp,humi=humi})
            if ok then
                 m:publish("test", json, 0, 0, function(client) print("sent") end)
            else
                 print("failed to encode!")
            end
--        print(string)
        
	elseif status == dht.ERROR_CHECKSUM then
		print( "DHT Checksum error." )
	elseif status == dht.ERROR_TIMEOUT then
		print( "DHT timed out." )
	end
end
